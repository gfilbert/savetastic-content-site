<?php

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php if ( ! empty( $refresh_url ) ) : ?>
	<script type="text/javascript">
		<?php // phpcs:ignore WordPress.Security.EscapeOutput ?>
		window.location.href = <?php echo SheKnows\Infuse\json_encode( $refresh_url ); ?>;
	</script>
<?php endif; ?>
