<?php

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// phpcs:disable WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
?>
<p>
	<label for="<?php echo esc_attr( $adTypeFieldId ); ?>">Ad type:</label>
	<select class="sheknows-infuse-adwidget-adtypes widefat" id="<?php echo esc_attr( $adTypeFieldId ); ?>" name="<?php echo esc_attr( $adTypeFieldName ); ?>">
	<?php foreach ( $adTypeOptions as $option ) : ?>
		<option value="<?php echo esc_attr( $option['value'] ); ?>" <?php selected( $option['value'], $currentType ); ?>><?php echo esc_html( $option['text'] ); ?></option>
	<?php endforeach; ?>
	</select>
</p>
<p>
	<input id="<?php echo esc_attr( $adCalloutId ); ?>" type="checkbox" name="<?php echo esc_attr( $adCalloutName ); ?>" value="1" <?php checked( $adCallout, 1 ); ?>>
	<label for="<?php echo esc_attr( $adCalloutId ); ?>">Add ADVERTISEMENT label</label>
</p>
