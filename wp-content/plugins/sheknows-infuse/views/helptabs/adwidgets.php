<?php

// Prevent direct file access
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<p>
	The <strong><code>SHE Media Ad</code></strong> widget allows for placement of ads in widget areas that are pre-
	defined by your theme, such as the sidebar. To place a <code>SHE Media Ad</code> in a widget area:
</p>
<ul>
	<li>Drag the <code>SHE Media Ad</code> widget from the left to a widget container on the right. Drop the new widget where you would like to place the ad.</li>
	<li>Once the widget has been placed, select an ad type from the widget's <code>Ad Type</code> dropdown.</li>
	<li>Click the <code>Save</code> button.</li>
</ul>
<p>You may also wish to place ads inline with your post content.  This is can be accomplished through shortcodes.  See the help tab on the post editing pages for more information.</p>
