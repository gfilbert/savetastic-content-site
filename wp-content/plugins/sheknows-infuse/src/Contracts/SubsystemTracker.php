<?php

namespace SheKnows\Infuse\Contracts;

interface SubsystemTracker {

	/**
	 * Called by the core Tracker module, obtaining data that each subsystem is
	 * responsible for.
	 *
	 * @return array
	 */
	public function requestTrackingData();
}
