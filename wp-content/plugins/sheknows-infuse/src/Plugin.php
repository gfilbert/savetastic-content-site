<?php

namespace SheKnows\Infuse;

/**
 * Class Plugin
 * Contains basic info on the plugin.
 *
 * @package SheKnows\Infuse
 */
class Plugin {

	/**
	 * The pretty name of the plugin.
	 *
	 * @var string
	 */
	protected $name;

	/**
	 * THe current version of the plugin.
	 *
	 * @var string
	 */
	protected $version;

	/**
	 * The base slug of the plugin.
	 *
	 * @var string
	 */
	protected $slug;

	/**
	 * The full path to the main plugin file.
	 *
	 * @var string
	 */
	protected $file;

	/**
	 * Plugin constructor.
	 *
	 * @param $name
	 * @param $version
	 * @param $slug
	 */
	public function __construct( $name, $version, $slug, $file ) {
		$this->name    = $name;
		$this->version = $version;
		$this->slug    = $slug;
		$this->file    = $file;
	}

	/**
	 * Get the name of the plugin.
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Get the version of the plugin.
	 *
	 * @return string
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * Get the base slug of the plugin.
	 *
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * Get the full path to the plugin file for this plugin.
	 *
	 * @return string
	 */
	public function getFile() {
		return $this->file;
	}

	/**
	 * Return the URL to the Infuse options page.
	 *
	 * @return string  The admin url for the options page.
	 */
	public function getOptionsPageUrl() {
		return admin_url( sprintf( 'options-general.php?page=%s', $this->getSlug() ) );
	}

}
