<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Contracts\SubsystemMetaBoxInput;
use SheKnows\Infuse\Plugin;

/**
 * Handles SHE Media Ads metabox
 *
 * Class MetaBox
 *
 * @package SheKnows\Infuse\Core
 */
class MetaBox {
	/**
	 * @var string
	 */
	private $metabox_key;

	/**
	 * @var string
	 */
	private $nonce_action;

	/**
	 * @var string
	 */
	private $nonce_name;

	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * @param Container $container
	 * @param Plugin $plugin
	 */
	public function __construct( Container $container, Plugin $plugin ) {
		$this->container    = $container;
		$this->nonce_action = $plugin->getSlug() . '_metabox_action';
		$this->nonce_name   = $plugin->getSlug() . '_metabox_nonce';
		$this->metabox_key  = $plugin->getSlug() . '_metabox';
	}

	/**
	 * Adds meta box to the sidebar.
	 *
	 * @param string $type
	 */
	public function addMetaBox( $type ) {
		add_meta_box(
			$this->metabox_key,
			__( 'SHE Media Ads', 'sheknows-infuse' ),
			array( $this, 'renderMetaBox' ),
			$type,
			'side'
		);
	}

	/**
	 * @param \WP_Post $post
	 */
	public function renderMetaBox( $post ) {
		$subsystemInputs = $this->container->getCallouts( 'SubsystemMetaBoxInput' );
		?>
		<div class="inside">
			<?php
			foreach ( $subsystemInputs as $subsystemInput ) {
				/** @var  SubsystemMetaBoxInput $input */
				$input = $this->container->resolve( $subsystemInput );
				$input->render( $post );
			}
			?>
		</div>
		<?php
		wp_nonce_field( $this->nonce_action, $this->nonce_name );
	}

	/**
	 * @param $post_id
	 */
	public function saveMetaData( $post_id ) {
		// Do not save autosaving data.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// Check user ability.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		if ( ! isset( $_POST[ $this->nonce_name ] ) || ! wp_verify_nonce( sanitize_key( $_POST[ $this->nonce_name ] ), $this->nonce_action ) ) { // Input var ok.
			return;
		}

		$subsystemInputs = $this->container->getCallouts( 'SubsystemMetaBoxInput' );
		foreach ( $subsystemInputs as $subsystemInput ) {
			/** @var  SubsystemMetaBoxInput $input */
			$input = $this->container->resolve( $subsystemInput );
			$input->saveMetaData( $post_id );
		}
	}
}
