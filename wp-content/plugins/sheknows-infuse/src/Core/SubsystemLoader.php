<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Contracts\SubsystemLoader as LoaderContract;

/**
 * Each subclass (within Subsystems/<Subsystem>/Loader) is instantiated at
 * startup
 *
 * Class SubsystemLoader
 *
 * @package SheKnows\Infuse\Core
 */
abstract class SubsystemLoader implements LoaderContract {
	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * Constructor.
	 *
	 * @param Container $container A reference to the plugin's IoC container.
	 */
	public function __construct( Container $container ) {
		$this->container = $container;
	}

	/**
	 * Stub for the subsystem's loader/bootstrapper method.
	 *
	 * @return void
	 */
	abstract public function load();
}
