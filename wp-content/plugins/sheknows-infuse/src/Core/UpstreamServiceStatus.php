<?php

namespace SheKnows\Infuse\Core;

/**
 * Detect the status of a service that is controlled upstream.
 *
 * @package SheKnows\Infuse\Core
 */
class UpstreamServiceStatus {
	/**
	 * The UpstreamCommonConfig instance.
	 *
	 * @var UpstreamCommonConfig
	 */
	protected $commonConfig;

	/**
	 * The UpstreamSiteConfig instance.
	 *
	 * @var UpstreamSiteConfig
	 */
	protected $siteConfig;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * Constructor.
	 *
	 * @param UpstreamCommonConfig $commonConfig
	 * @param UpstreamSiteConfig $siteConfig
	 * @param ErrorLogging $errorLogging
	 */
	public function __construct( UpstreamCommonConfig $commonConfig, UpstreamSiteConfig $siteConfig, ErrorLogging $errorLogging ) {
		$this->commonConfig = $commonConfig;
		$this->siteConfig   = $siteConfig;
		$this->errorLogging = $errorLogging;
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function isEnabled( $name ) {
		try {
			$defaults  = $this->commonConfig->get( 'service_defaults' );
			$overrides = $this->siteConfig->get( 'service_overrides' );
		} catch ( \Exception $exception ) {
			$this->errorLogging->record( $exception );
			return false;
		}

		if ( is_array( $defaults ) && isset( $defaults[ $name ] ) ) {
			$default = $defaults[ $name ];
		} else {
			$default = 0;
		}

		if ( -1 === $default ) {
			return false;
		}

		if ( is_array( $overrides ) && isset( $overrides[ $name ] ) ) {
			if ( 1 === $overrides[ $name ] ) {
				return true;
			}

			if ( -1 === $overrides[ $name ] ) {
				return false;
			}
		}

		return 1 === $default ? true : false;
	}
}
