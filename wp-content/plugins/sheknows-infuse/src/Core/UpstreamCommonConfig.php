<?php

namespace SheKnows\Infuse\Core;

/**
 * Retrieves and provides access to the common upstream config options.
 *
 * @package SheKnows\Infuse\Core
 */
class UpstreamCommonConfig extends UpstreamConfig {
	/**
	 * Constructor.
	 *
	 * @param CacheInfuseCommonData $cache
	 */
	public function __construct( CacheInfuseCommonData $cache ) {
		$this->cache = $cache;
	}
}
