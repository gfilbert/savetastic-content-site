<?php

namespace SheKnows\Infuse\Core;

/**
 * Checks and updates cached infuse.json.
 *
 * @package SheKnows\Infuse\Core
 */
class CacheInfuseJson extends CacheBoomerangSiteFile {
	/**
	 * Identifier for the cache transient for this file.
	 *
	 * @var string
	 */
	protected $transientName = 'json';

	/**
	 * If there are problems retrieving this file, back off for 5 minutes.
	 *
	 * @var int
	 */
	protected $backoff = 300;

	/**
	 * Minimum lifetime of transients. Prevents excessive retrievals.
	 *
	 * @var int
	 */
	protected $minimumExpiry = 900;

	/**
	 * {@inheritdoc}
	 */
	public function getUrl() {
		$baseUrl = $this->getBaseUrl();

		return $baseUrl ? sprintf( '%s/infuse.json', $baseUrl ) : false;
	}
}
