<?php

namespace SheKnows\Infuse\Core;

use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Contracts\SubsystemTracker as TrackerContract;

abstract class SubsystemTracker implements TrackerContract {
	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * Constructor.
	 *
	 * @param Options $options
	 * @param Plugin  $plugin
	 */
	public function __construct( Options $options, Plugin $plugin ) {
		$this->options = $options;
		$this->plugin  = $plugin;
	}

	/**
	 * Called by the core Tracker module, obtaining data that each subsystem is
	 * responsible for.
	 *
	 * @return array
	 */
	abstract public function requestTrackingData();
}
