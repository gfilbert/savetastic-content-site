<?php

namespace SheKnows\Infuse\Core;

/**
 * Retrieves and caches a file from the configured Boomerang path.
 *
 * @package SheKnows\Infuse\Core
 */
abstract class CacheBoomerangFile {
	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * Portion of the transient name that uniquely identifies this cached file.
	 * Subclasses should set this.
	 *
	 * @var string
	 */
	protected $transientName;

	/**
	 * Set a hard expiration date for the cache transient.  This allows us to soft
	 * expire the file based on the cache headers received when it was retrieved,
	 * but also keep it around in case there are issues re-retrieving it after it
	 * soft expires.  Default is to hard-cache for 1 week.
	 *
	 * @var int
	 */
	protected $hardExpire = 604800;

	/**
	 * Default soft expiration time if no cache headers were present in the
	 * response.  Default is 1 day.
	 *
	 * @var int
	 */
	protected $softExpire = 86400;

	/**
	 * Minimum lifetime of transients. Prevents excessive retrievals.
	 *
	 * @var int
	 */
	protected $minimumExpiry;

	/**
	 * Determines whether to set a backoff timer if there are problems retrieving
	 * the upstream file.  If false, no backoff transient is set.  Otherwise, if
	 * an integer is set here, then it specifies the backoff time in seconds.  The
	 * default is not to set a backoff transient.
	 *
	 * @var boolean|int
	 */
	protected $backoff = false;

	/**
	 * Should be implemented in all the subclasses and return the URL
	 * to retrieve the data from, or false in case we're not able to build the URL.
	 *
	 * @return bool|string
	 */
	abstract public function getUrl();

	/**
	 * Constructor.
	 *
	 * @param Options  $options
	 */
	public function __construct( Options $options ) {
		$this->options = $options;
	}

	/**
	 * Checks to see if the upstream file has been cached.  If it has, and it
	 * hasn't soft-expired yet, return the cached entry.  Otherwise attempt to
	 * retrieve a fresh copy.
	 * @param  boolean $force  Set to true if the caller would like to try to
	 *                         retrieve a fresh copy of the upstream file.
	 * @return boolean|array   False if there was a failure retrieving the file
	 *                         and the cache was not set, otherwise an array
	 *                         with two keys: 'expires' gives the soft expiry of
	 *                         the content based on the upstream cache headers,
	 *                         and 'content' is the body of the most recent
	 *                         response received.  This may be the last cached
	 *                         content even if $force is true if the retrieval
	 *                         failed.
	 */
	public function get( $force = false ) {
		$cache = get_transient( $this->transientName() );

		if ( $force || ! is_array( $cache ) || $cache['expires'] < time() ) {
			$newCache = $this->retrieve();

			if ( $newCache ) {
				$cache = $newCache;
			}
		}

		// Always return what we got from the cache.  In a case where the cached
		// entry had soft-expired, but the retrieval failed for some reason,
		// this allows us to return the last known value until the cached entry
		// fully expires from the cache.
		return $cache;
	}

	/**
	 * Returns base URL to retrieve the data.
	 *
	 * @return bool|string
	 */
	protected function getBaseUrl() {
		return 'https://ads.blogherads.com';
	}

	/**
	 * Attempts to pull the upstream file.
	 *
	 * @return boolean|array
	 */
	protected function retrieve() {
		if ( $this->backoff && get_transient( $this->transientName( true ) ) ) {
			// If backoff timer is present.
			return false;
		}

		if ( $this->backoff ) {
			set_transient( $this->transientName( true ), true, $this->backoff );
		}

		$url = $this->getUrl();

		if ( empty( $url ) ) {
			return false;
		}

		$response = wp_remote_get( $url );

		if ( wp_remote_retrieve_response_code( $response ) !== 200 ) {
			return false;
		}

		if ( $this->backoff ) {
			delete_transient( $this->transientName( true ) );
		}

		return $this->cache( $response );
	}

	/**
	 * Caches the response.
	 *
	 * @param  array  $response
	 * @return array
	 */
	protected function cache( $response ) {
		$transient = array(
			'content' => wp_remote_retrieve_body( $response ),
			'expires' => $this->expires( $response ),
		);

		// Keep the transient for up to the hard expiration time if possible so that
		// we have a fallback in case anything goes wrong upstream.
		set_transient( $this->transientName(), $transient, $this->hardExpire );

		return $transient;
	}

	/**
	 * Determine the soft expiration timestamp based on cache headers that might
	 * be present in the response.
	 *
	 * @param  array   $response
	 * @return integer
	 */
	protected function expires( $response ) {
		$headers = $response['headers'];

		// If nothing else, use softExpire.
		$expires = $this->softExpire;

		// If there is a cache-control header, use max-age...
		if ( isset( $headers['cache-control'] ) ) {
			$cacheControl           = $headers['cache-control'];
			$cacheControlDirectives = explode( ',', $cacheControl );
			$directiveCount         = count( $cacheControlDirectives );

			for ( $i = 0; $i < $directiveCount; $i++ ) {
				$nextDir = trim( $cacheControlDirectives[ $i ] );
				if ( strpos( $nextDir, 'max-age' ) === 0 ) {
					$maxAge = str_replace( 'max-age=', '', $nextDir );

					if ( is_numeric( $maxAge ) ) {
						$expires = (int) $maxAge;
					}

					break;
				}
			}
		}

		// ...unless a minimum expiry has been set and it is more than expires.
		$expires = $this->checkMinimumExpiry( $expires );

		// Expires $expires from now.
		return $expires + time();
	}

	/**
	 * If a minimum expiry is present (e.g. infuse.json), ensure that a resource
	 * will be cached for at least this minimal amount of time.
	 *
	 * @param integer $expiry
	 *
	 * @return integer
	 */
	protected function checkMinimumExpiry( $expiry ) {
		if ( isset( $this->minimumExpiry ) ) {
			return max( $expiry, $this->minimumExpiry );
		}
		return $expiry;
	}

	/**
	 * Return a transient name.
	 *
	 * @param  boolean  $backoff  True if this is the backoff transient.
	 * @return string
	 */
	protected function transientName( $backoff = false ) {
		$extra = $backoff ? '_b' : '';

		return sprintf( 'sheknows_infuse_%s%s', $this->transientName, $extra );
	}
}
