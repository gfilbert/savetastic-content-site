<?php

namespace SheKnows\Infuse\Core;

/**
 * Instantiates and returns instances of the View class
 *
 * Class ViewFactory
 *
 * @package SheKnows\Infuse\Core
 */
class ViewFactory {

	/**
	 * The base directory where views are located.
	 *
	 * @var string
	 */
	protected $baseDirectory;

	/**
	 * ViewFactory constructor.
	 *
	 * @param string $baseDirectory
	 */
	public function __construct( $baseDirectory ) {
		$this->baseDirectory = $baseDirectory;
	}

	/**
	 * Create and return a new View instance.
	 *
	 * @param string $name  The filename of the view to be rendered, relative to
	 *                      the views base directory and without the .php
	 *                      extension.  Example: 'admin/settings' would result
	 *                      in rendering 'plugin-dir/views/admin/settings.php'.
	 * @param array  $vars  A list of variables to be passed to the view.
	 *
	 * @return View
	 */
	public function make( $name, $vars = array() ) {
		return new View( $this->baseDirectory, $name, $vars );
	}
}
