<?php

namespace SheKnows\Infuse;

/**
 * Simple json_encode intermediary that handles using wp_json_encode on WP 4.1+
 * and straight json_encode on WP < 4.1.  This function is simple and does not
 * accept any of the other json_encode parameters besides the data to be
 * encoded.
 *
 * @param  mixed   $data  The data to be encoded.
 * @return string         The encoded data.
 */
function json_encode( $data ) {
	if ( function_exists( 'wp_json_encode' ) ) {
		// WordPress >= 4.1.0
		return wp_json_encode( $data );
	} else {
		// WordPress < 4.1.0
		// phpcs:ignore WordPress.WP.AlternativeFunctions
		return \json_encode( $data );
	}
}
