<?php

namespace SheKnows\Infuse\Subsystems\Shortcodes;

use SheKnows\Infuse\Core\AdTypes;
use SheKnows\Infuse\Core\ErrorLogging;
use SheKnows\Infuse\Core\UpstreamCommonConfig;
use SheKnows\Infuse\Core\UpstreamSiteConfig;
use SheKnows\Infuse\Core\Options;
use SheKnows\Infuse\Core\ViewFactory;
use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Subsystems\HeaderCode\DisableAds;

class ShortCodeHandler {
	/**
	 * number of shortcode ads counted (starting at 0)
	 *
	 * @var int
	 */
	protected $adCount = 0;

	/**
	 * @var ViewFactory
	 */
	protected $viewFactory;

	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var AdTypes
	 */
	protected $adTypes;

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var DisableAds
	 */
	protected $disableAds;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * ShortCodeHandler constructor.
	 *
	 * @param ViewFactory  $viewFactory
	 * @param Options      $options
	 */
	public function __construct( ViewFactory $viewFactory, Options $options, AdTypes $adTypes, Plugin $plugin, DisableAds $disableAds, ErrorLogging $errorLogging ) {
		$this->viewFactory  = $viewFactory;
		$this->options      = $options;
		$this->adTypes      = $adTypes;
		$this->plugin       = $plugin;
		$this->disableAds   = $disableAds;
		$this->errorLogging = $errorLogging;
	}

	/**
	 * All shortcode ads are processed here.
	 *
	 * @param array $args
	 * @param string $tag
	 * @return string
	 */
	public function handle( $args, $tag ) {
		$insertHeader  = $this->options->get( 'header_code_enabled' );
		$boomerangPath = $this->options->get( 'boomerang_path' );

		if ( $this->disableAds->getValue() || ! $insertHeader || empty( $boomerangPath ) ) {
			return '';
		}

		$defaults = array(
			'type'       => null,
			'divid'      => null,
			'class'      => '',
			'float'      => false,
			'ad-callout' => false,
		);

		$attributes = shortcode_atts( $defaults, $args, $tag );

		// Must have a valid type.
		if ( ! isset( $attributes['type'] ) || ! $this->adTypes->getTypeByName( $attributes['type'] ) ) {
			return '';
		}

		if ( empty( $attributes['divid'] ) ) {
			// Assign a default id using the counter if not provided by the user.
			$attributes['divid'] = $this->plugin->getSlug() . '-shortcode-ad-' . $this->incrementAdCount();
		} else {
			// Ensure the supplied divid is lowercase.
			$attributes['divid'] = strtolower( $attributes['divid'] );
		}

		if ( is_string( $attributes['float'] ) ) {
			$attributes['float'] = strtolower( $attributes['float'] );
		}

		$vars = array(
			'type'    => $attributes['type'],
			'classes' => $this->processClasses( $attributes ),
			'divid'   => $attributes['divid'],
			'style'   => '',
		);

		$info = $this->adTypes->getTypeByName( $attributes['type'] );
		if ( ! empty( $info['style'] ) ) {
			$vars['style'] = $info['style'];
		}

		// Unlike other output function, shortcode handlers are expected to
		// *return* their output to the caller.
		return $this->viewFactory->make( 'ad', $vars )->render();
	}

	/**
	 * Add any default and floating classes to the provided class string.
	 *
	 * @param  array   $attributes  Array containing shortcode attributes that need to be processed.
	 * @return string               The new class string.
	 */
	protected function processClasses( $attributes ) {

		// Ensure class names are lowercase before splitting them up.
		$list   = array_filter( explode( ' ', strtolower( $attributes['class'] ) ) );
		$list[] = $this->plugin->getSlug() . '-shortcode-ad';

		if ( 'left' === $attributes['float'] ) {
			$list[] = 'sheknows-infuse-ad-float-left';
		} elseif ( 'right' === $attributes['float'] ) {
			$list[] = 'sheknows-infuse-ad-float-right';
		}

		if ( 'enabled' === $attributes['ad-callout'] ) {
			$list[] = 'sheknows-infuse-ad-callout';
		}

		return implode( ' ', $list );
	}

	/**
	 * Turnstyle. Bumps the count by 1 and returns the new count.
	 *
	 * @return int  Number of ads after function was called.
	 */
	protected function incrementAdCount() {
		return ++ $this->adCount;
	}
}
