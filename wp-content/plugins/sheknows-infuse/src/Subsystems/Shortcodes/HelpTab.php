<?php

namespace SheKnows\Infuse\Subsystems\Shortcodes;

use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Core\AdTypes;
use SheKnows\Infuse\Core\ViewFactory;

/**
 * Adds help tab content where applicable.
 *
 * Class HelpTab
 *
 * @package SheKnows\Infuse\Subsystems\Shortcodes
 */
class HelpTab {
	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var ViewFactory
	 */
	protected $viewFactory;

	/**
	 * @var AdTypes
	 */
	protected $adTypes;

	/**
	 * HelpTab constructor.
	 *
	 * @param Plugin       $plugin
	 * @param ViewFactory  $viewFactory
	 * @param AdTypes      $adTypes
	 */
	public function __construct( Plugin $plugin, ViewFactory $viewFactory, AdTypes $adTypes ) {
		$this->plugin      = $plugin;
		$this->viewFactory = $viewFactory;
		$this->adTypes     = $adTypes;
	}

	/**
	 * Show the help tab on the appropriate screens.
	 */
	public function show() {
		$screen = get_current_screen();
		$args   = array( 'adtypes' => array() );

		foreach ( $this->adTypes->getAllTypes() as $type => $info ) {
			$args['adtypes'][] = sprintf(
				'<code>%s</code> - %s%s',
				esc_html( $type ),
				esc_html( $info['friendlyName'] ),
				isset( $info['sizes'] ) ? sprintf( ' (%s)', esc_html( implode( ', ', $info['sizes'] ) ) ) : ''
			);
		}

		if ( $screen ) {
			$screen->add_help_tab(
				array(
					'id'       => $this->plugin->getSlug() . '-ad-shortcodes',
					'title'    => 'SHE Media Ad Shortcodes',
					'content'  => $this->viewFactory->make( 'helptabs/shortcodes', $args )->render(),
					'priority' => 20,
				)
			);
		}
	}
}
