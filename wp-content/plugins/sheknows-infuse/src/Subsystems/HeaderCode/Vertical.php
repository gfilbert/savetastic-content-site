<?php

namespace SheKnows\Infuse\Subsystems\HeaderCode;

use SheKnows\Infuse\Core\ErrorLogging;
use SheKnows\Infuse\Core\SubsystemMetaBoxInput;
use SheKnows\Infuse\Core\UpstreamCommonConfig;
use SheKnows\Infuse\Plugin;

class Vertical extends SubsystemMetaBoxInput {

	const KEY = 'metabox_post_vertical';

	/**
	 * @var UpstreamCommonConfig
	 */
	protected $upstreamCommonConfig;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * @param Plugin $plugin
	 * @param UpstreamCommonConfig $upstreamCommonConfig
	 * @param ErrorLogging $errorLogging
	 *
	 * @throws \Exception
	 */
	public function __construct( Plugin $plugin, UpstreamCommonConfig $upstreamCommonConfig, ErrorLogging $errorLogging ) {
		parent::__construct( $plugin );

		$this->upstreamCommonConfig = $upstreamCommonConfig;
		$this->errorLogging         = $errorLogging;
	}

	/**
	 * {@inheritdoc}
	 */
	public function render( $post ) {
		$type = get_post_type_object( $post->post_type );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->key ); ?>">
				<?php echo esc_html( $type->labels->singular_name ); ?> vertical:
			</label>
			<select
				style="width: 100%"
				id="<?php echo esc_attr( $this->key ); ?>"
				name="<?php echo esc_attr( $this->key ); ?>"
			>
				<option value="">Site default</option>
				<?php foreach ( $this->getVerticals() as $vertical ) { ?>
					<option
						value="<?php echo esc_attr( sanitize_title( $vertical ) ); ?>"
						<?php selected( sanitize_title( $vertical ), $this->getValue( $post->ID ) ); ?>
					>
						<?php echo esc_html( $vertical ); ?>
					</option>
				<?php } ?>
			</select>
		</p>
		<?php
	}

	/**
	 * {@inheritdoc}
	 */
	public function saveMetaData( $post_id ) {
		// Nonce verification is happening in caller \SheKnows\Infuse\Core\MetaBox::saveMetaData
		// phpcs:ignore WordPress.Security.NonceVerification
		$metaValue   = isset( $_POST[ $this->key ] ) ? $_POST[ $this->key ] : false;
		$verticals   = array_map( 'sanitize_title', $this->getVerticals() );
		$verticals[] = '';
		if ( isset( $metaValue ) && in_array( $metaValue, $verticals, true ) ) {
			update_post_meta( $post_id, $this->key, $metaValue );
		}
	}

	/**
	 * Returns the list of available verticals.
	 *
	 * @return array
	 */
	private function getVerticals() {
		try {
			$verticals = $this->upstreamCommonConfig->get( 'boomerang_verticals' );
		} catch ( \Exception $exception ) {
			$this->errorLogging->record( $exception );
		}

		if ( isset( $verticals ) && is_array( $verticals ) ) {
			return $verticals;
		}

		return array();
	}
}
