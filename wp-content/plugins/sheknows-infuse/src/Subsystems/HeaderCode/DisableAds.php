<?php

namespace SheKnows\Infuse\Subsystems\HeaderCode;

use SheKnows\Infuse\Core\SubsystemMetaBoxInput;

class DisableAds extends SubsystemMetaBoxInput {

	const KEY = 'metabox_post_disable_ads';

	/**
	 * {@inheritdoc}
	 */
	public function render( $post ) {
		$type = get_post_type_object( $post->post_type );
		?>
		<p>
			<input
				id="<?php echo esc_attr( $this->key ); ?>"
				type="checkbox"
				name="<?php echo esc_attr( $this->key ); ?>"
				value="1"
				<?php checked( $this->getValue( $post->ID ) ); ?>
			/>
			<label for="<?php echo esc_attr( $this->key ); ?>">
				Disable ads on this <?php echo esc_html( $type->labels->singular_name ); ?>
			</label>
		</p>
		<?php
	}

	/**
	 * {@inheritdoc}
	 */
	public function saveMetaData( $post_id ) {
		// Nonce verification is happening in caller \SheKnows\Infuse\Core\MetaBox::saveMetaData
		// phpcs:ignore WordPress.Security.NonceVerification
		if ( isset( $_POST[ $this->key ] ) ) {
			update_post_meta( $post_id, $this->key, true );
		} else {
			delete_post_meta( $post_id, $this->key );
		}
	}
}
