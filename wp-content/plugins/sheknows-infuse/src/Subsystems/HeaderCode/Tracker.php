<?php

namespace SheKnows\Infuse\Subsystems\HeaderCode;

use SheKnows\Infuse\Core\SubsystemTracker;

class Tracker extends SubsystemTracker {

	/**
	 * Returns Boomerang path and whether header code is enabled for tracking purposes.
	 *
	 * @return array
	 */
	public function requestTrackingData() {
		$trackingData = array(
			'header_code_enabled' => (bool) $this->options->get( 'header_code_enabled' ),
		);

		return $trackingData;
	}
}
