<?php

namespace SheKnows\Infuse\Subsystems\ADmantX;

use SheKnows\Infuse\Contracts\CachedDataProvider;

class Fetch implements CachedDataProvider {
	const API_KEY    = 'dcde31e32d21f5432ab192ee50f9e0a8ef294bab4778ada95dfabeb949b2b9ce';
	const REMOTE_URI = 'https://async01.admantx.com/admantx/service';

	/**
	 * Fetches the ADmantX contextual segments (called "admants") for the
	 * current post/page and returns them in the format required by the
	 * DataCache.
	 *
	 * @param array $arguments
	 * @return array ['success' => boolean, 'data' => [...'admants']]
	 */
	public function generateData( $arguments ) {

		$currentUrl = ! empty( $arguments['currentUrl'] ) ? $arguments['currentUrl'] : '';
		$postId     = ! empty( $arguments['postId'] ) ? $arguments['postId'] : 0;

		if ( empty( $currentUrl ) && ! empty( $postId ) ) {
			$currentUrl = get_permalink( $postId );
		}

		// If there is no valid URL, return fail not ready error
		if ( empty( $currentUrl ) ) {
			return array(
				'success' => false,
				'data'    => array( 'fail', 'fail_not_ready' ),
			);
		}

		$request = rawurlencode(
			\SheKnows\Infuse\json_encode(
				array(
					'key'       => self::API_KEY,
					'decorator' => 'template.pmc',
					'filter'    => array( 'default' ),
					'method'    => 'descriptor',
					'mode'      => 'async',
					'type'      => 'URL',
					'body'      => rawurlencode( $currentUrl ),
				)
			)
		);

		$fetchUrl = add_query_arg( 'request', $request, self::REMOTE_URI );
		$response = wp_safe_remote_get( $fetchUrl );

		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
			return array(
				'success' => false,
				'data'    => array( 'fail', 'fail_error' ),
			);
		}

		$body = wp_remote_retrieve_body( $response );

		$body = preg_replace(
			array( '/^\\s*pmc_admantx\\.callback\\(/', '/\\);\\s*$/' ),
			'',
			$body
		);

		$json = json_decode( $body, false );

		if ( ! empty( $json ) && isset( $json->status ) ) {
			if ( 'ok' === strtolower( $json->status ) ) {

				if ( isset( $json->admants ) && ! is_array( $json->admants ) ) {
					return array(
						'success' => false,
						'data'    => array( 'fail', 'fail_error' ),
					);
				} elseif ( ! isset( $json->admants ) || 0 === count( $json->admants ) ) {
					return array(
						'success' => true,
						'data'    => array( 'none' ),
					);
				}

				return array(
					'success' => true,
					'data'    => $json->admants,
				);

			} elseif ( 'pending' === strtolower( $json->status ) ) {
				return array(
					'success' => false,
					'data'    => array( 'fail', 'fail_pending' ),
				);
			}
		}

		// Catch all, everything else return fail error
		return array(
			'success' => false,
			'data'    => array( 'fail', 'fail_error' ),
		);

	}

}
