<?php

namespace SheKnows\Infuse\Subsystems\ADmantX;

use SheKnows\Infuse\Core\DataCacheFactory;
use SheKnows\Infuse\Core\UpstreamSiteConfig;
use SheKnows\Infuse\Core\UpstreamCommonConfig;

class Admants {

	/**
	 * @var DataCacheFactory
	 */
	protected $dataCacheFactory;

	/**
	 * @var Fetch
	 */
	protected $fetch;

	/**
	 * @var UpstreamSiteConfig
	 */
	protected $siteConfig;

	/**
	 * @var UpstreamCommonConfig
	 */
	protected $commonConfig;

	/**
	 * @constructor
	 * @param DataCacheFactory $dataCacheFactory
	 * @param Fetch            $fetch
	 */
	public function __construct( DataCacheFactory $dataCacheFactory, Fetch $fetch, UpstreamSiteConfig $siteConfig, UpstreamCommonConfig $commonConfig ) {
		$this->dataCacheFactory = $dataCacheFactory;
		$this->fetch            = $fetch;
		$this->siteConfig       = $siteConfig;
		$this->commonConfig     = $commonConfig;
	}

	/**
	 * Return the admants for this request.
	 *
	 * @param array $vars  The extra variables from the sheknows_infuse_global_targeting filter.
	 * @return string[]
	 */
	public function getAdmants( $vars ) {
		// Content on list pages changes often and is not targeted via contextual information anyway.
		if ( is_home() || is_front_page() || is_archive() || ! empty( $vars['disable_ads'] ) ) {
			return array( 'fail', 'fail_disabled' );
		} elseif ( ! empty( $vars['sponsored'] ) ) {
			return array( 'fail', 'fail_sponsored' );
		}

		$currentUrl = home_url( strtok( $_SERVER['REQUEST_URI'], '?' ) );
		$postId     = is_singular() ? get_queried_object_id() : false;

		$defaultTTL  = $this->commonConfig->get( 'admantx_default_cache_ttl' );
		$overrideTTL = $this->siteConfig->get( 'admantx_infuse_cache_lifetime' );
		$cacheTTL    = ( false !== $overrideTTL ) ? $overrideTTL : $defaultTTL;

		$cachedData = $this->dataCacheFactory->make(
			$this->fetch,
			array(
				'postId'     => $postId,
				'currentUrl' => $currentUrl,
			),
			$cacheTTL,
			60,
			array( 'fail', 'fail_error' ),
			array( 'fail', 'fail_not_ready' )
		);

		return $cachedData->get();
	}
}
