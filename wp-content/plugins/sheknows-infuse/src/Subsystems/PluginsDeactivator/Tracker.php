<?php

namespace SheKnows\Infuse\Subsystems\PluginsDeactivator;

use SheKnows\Infuse\Core\Options;
use SheKnows\Infuse\Core\SubsystemTracker;
use SheKnows\Infuse\Plugin;

class Tracker extends SubsystemTracker {
	/**
	 * @var Deactivator
	 */
	private $deactivator;

	public function __construct( Options $options, Plugin $plugin, Deactivator $deactivator ) {
		parent::__construct( $options, $plugin );

		$this->deactivator = $deactivator;
	}

	/**
	 * Returns the list of enabled competing plugins for tracking purposes.
	 *
	 * @return array
	 */
	public function requestTrackingData() {
		$trackingData = array(
			'infuse_competing_plugins' => array_keys( $this->deactivator->getPlugins() ),
		);

		return $trackingData;
	}
}
