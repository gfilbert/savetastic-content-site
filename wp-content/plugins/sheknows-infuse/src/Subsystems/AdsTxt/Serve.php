<?php

namespace SheKnows\Infuse\Subsystems\AdsTxt;

use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Core\Options;

/**
 * Class Serve
 *
 * @package SheKnows\Infuse\Subsystems\AdsTxt
 */
class Serve {
	/**
	 * @var Cache
	 */
	protected $cache;

	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * Serve constructor.
	 *
	 * @param Cache  $cache
	 */
	public function __construct( Cache $cache, Options $options, Plugin $plugin ) {
		$this->cache   = $cache;
		$this->options = $options;
		$this->plugin  = $plugin;
	}

	/**
	 * Serves the ads.txt file.
	 */
	public function serve() {
		$cache = $this->cache->get();

		if ( ! $cache || ! is_string( $cache['content'] ) ) {
			return;
		}

		status_header( 200 );
		header( 'Content-Type: text/plain; charset=utf-8' );

		$generator = sprintf( '%s/%s', $this->plugin->getSlug(), $this->plugin->getVersion() );
		header( 'Generator: ' . $generator );

		// phpcs:ignore WordPress.Security.EscapeOutput
		echo $cache['content'] . "\n" . $this->options->get( 'adstxt_extra_lines' );
		exit();
	}
}
