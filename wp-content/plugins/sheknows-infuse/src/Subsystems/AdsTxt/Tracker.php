<?php

namespace SheKnows\Infuse\Subsystems\AdsTxt;

use SheKnows\Infuse\Core\SubsystemTracker;

class Tracker extends SubsystemTracker {

	/**
	 * Returns ads.txt tracking data.
	 *
	 * @return array
	 */
	public function requestTrackingData() {
		$extraAdsTxtLines = $this->options->get( 'adstxt_extra_lines' );
		$trackingData     = array(
			'ads_txt_enabled'           => (bool) $this->options->get( 'adstxt_enabled' ),
			'has_extra_ads_txt_entries' => trim( $extraAdsTxtLines ) === '' ? false : true,
		);

		return $trackingData;
	}
}
