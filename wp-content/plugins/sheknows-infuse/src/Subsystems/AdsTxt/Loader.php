<?php

namespace SheKnows\Infuse\Subsystems\AdsTxt;

use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Loads required classes for ads.txt cacheing and serving
 *
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\AdsTxt
 */
class Loader extends SubsystemLoader {
	/**
	 * Record if we ran the ads.txt hook since we add it to multiple actions.
	 *
	 * @var boolean
	 */
	protected $hookRan = false;

	/**
	 * Initializes binding relevant classes and relevant WordPress API hooks.
	 * Note that while bound classes may not load if not required
	 */
	public function load() {
		$this->bindSubsystemClasses();
		$this->registerOptions();
		$this->registerSubsystemHooks();
	}

	/**
	 * Makes relevant classes available at runtime. If class has not been
	 * instantiated, the class is instantiated (with dependencies added through
	 * the constructor) and handed over. If the class has been instantiated,
	 * the class is handed over.
	 */
	protected function bindSubsystemClasses() {
		$this->container->bind( __NAMESPACE__ . '\Admin', array( $this, 'bindAdmin' ), 'SubsystemAdmin' );
		$this->container->bind( __NAMESPACE__ . '\Cache', array( $this, 'bindCache' ) );
		$this->container->bind( __NAMESPACE__ . '\Serve', array( $this, 'bindServe' ) );
		$this->container->bind( __NAMESPACE__ . '\Tracker', array( $this, 'bindTracker' ), 'SubsystemTracker' );
	}

	/**
	 * Register this subsystem's settings.
	 */
	protected function registerOptions() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$options->register( 'adstxt_enabled', true );
		$options->register( 'adstxt_extra_lines', '', true );
	}

	/**
	 * Adds hooks to the WordPress API. Delays serving of ads.txt until after plugins are loaded.
	 */
	public function registerSubsystemHooks() {
		// We want this to run as soon as possible, but there are some corner
		// cases plugins_loaded has already run by the time Infuse gets loaded.
		// For this reason, we add it to both hooks and just track when it runs
		// so that it doesn't unnecessarily run twice.
		add_action( 'plugins_loaded', array( $this, 'maybeServeAdsTxt' ) );
		add_action( 'init', array( $this, 'maybeServeAdsTxt' ) );
	}

	/**
	 * Constructs the Admin class for the container.
	 *
	 * @return Admin
	 */
	public function bindAdmin() {
		$options              = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin               = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$coreAdmin            = $this->container->resolve( 'SheKnows\Infuse\Core\Admin' );
		$pluginsHelper        = $this->container->resolve( 'SheKnows\Infuse\Core\PluginsHelper' );
		$upstreamCommonConfig = $this->container->resolve( 'SheKnows\Infuse\Core\UpstreamCommonConfig' );
		$errorLogging         = $this->container->resolve( 'SheKnows\Infuse\Core\ErrorLogging' );

		return new Admin( $options, $plugin, $coreAdmin, $pluginsHelper, $upstreamCommonConfig, $errorLogging );
	}

	/**
	 * Constructs cache class
	 *
	 * @return Cache
	 */
	public function bindCache() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );

		return new Cache( $options );
	}

	/**
	 * Constructs serve class
	 *
	 * @return Serve
	 */
	public function bindServe() {
		$cache   = $this->container->resolve( __NAMESPACE__ . '\Cache' );
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin  = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Serve( $cache, $options, $plugin );
	}

	/**
	 * Constructs tracker class
	 *
	 * @return Tracker
	 */
	public function bindTracker() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin  = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Tracker( $options, $plugin );
	}

	/**
	 * If settings permit, trigger loading of the Serve class to serve ads.txt
	 */
	public function maybeServeAdsTxt() {
		if ( $this->hookRan ) {
			return;
		}

		$this->hookRan = true;

		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$enabled = $options->get( 'adstxt_enabled' );
		$path    = $options->get( 'boomerang_path' );

		// We immediately check to see if ads.txt was requested.  If it was,
		// there's no reason to continue, so we just print the ads.txt content
		// and exit.
		if ( $enabled && ! empty( $path ) ) {
			// This is a safe call to parse_url across PHP versions, since
			// site_url always sets the scheme.  Also, wp_parse_url itself is
			// not a stable API prior to WP 4.7.
			// phpcs:ignore WordPress.WP.AlternativeFunctions
			$root  = preg_quote( trailingslashit( parse_url( site_url(), PHP_URL_PATH ) ), '|' );
			$regex = sprintf( '|^%sads\.txt(\?.*)?$|i', $root );

			if ( preg_match( $regex, $_SERVER['REQUEST_URI'] ) ) {
				$serve = $this->container->resolve( __NAMESPACE__ . '\Serve' );
				$serve->serve();
			}
		}
	}
}
