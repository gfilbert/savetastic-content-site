<?php

namespace SheKnows\Infuse\Subsystems\AdWidget;

use SheKnows\Infuse\Core\Container;
use SheKnows\Infuse\Core\SubsystemLoader;

/**
 * Handles loading of classes required for widget functionality.
 *
 * Class Loader
 *
 * @package SheKnows\Infuse\Subsystems\AdWidget
 */
class Loader extends SubsystemLoader {

	/**
	 * Enables adherance to dependency injection while also sticking to standard
	 * WordPress flow for initializing a widget.
	 *
	 * @var Container
	 */
	protected static $staticContainer;

	/**
	 * Returns the container instance that the AdWidget main class can work with
	 * to initialize other classes.
	 *
	 * @return Container
	 */
	public static function getContainer() {
		return self::$staticContainer;
	}

	/**
	 * Loader constructor.
	 *
	 * @param Container  $container
	 */
	public function __construct( Container $container ) {
		parent::__construct( $container );
		self::$staticContainer = $container;
	}

	/**
	 * Initializes binding relevant classes and relevant WordPress API hooks.
	 */
	public function load() {
		$this->bindSubsystemClasses();
		$this->registerSubsystemHooks();
	}

	/**
	 * Bind our classes into the container.
	 */
	protected function bindSubsystemClasses() {
		$this->container->bind( __NAMESPACE__ . '\WidgetAdmin', array( $this, 'bindWidgetAdminProcessor' ) );
		$this->container->bind( __NAMESPACE__ . '\WidgetPublic', array( $this, 'bindWidgetPublicProcessor' ) );
		$this->container->bind( __NAMESPACE__ . '\HelpTab', array( $this, 'bindHelpTab' ) );
		$this->container->bind( __NAMESPACE__ . '\Tracker', array( $this, 'bindTracker' ), 'SubsystemTracker' );
	}

	/**
	 * Adds hooks to the WordPress API. Brings up widget main class when
	 * widgets initialize; also populates ad types when an admin page loads
	 */
	protected function registerSubsystemHooks() {
		add_action( 'widgets_init', array( $this, 'initializeWidget' ) );

		if ( is_admin() ) {
			add_action( 'load-widgets.php', array( $this, 'addHelp' ) );
		}
	}

	/**
	 * Register our main Widget class with WordPress when Infuse is active.
	 */
	public function initializeWidget() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );

		$boomerangPath = $options->get( 'boomerang_path' );
		$headerEnabled = $options->get( 'header_code_enabled' );

		if ( ! empty( $boomerangPath ) && $headerEnabled ) {
			register_widget( __NAMESPACE__ . '\Widget' );
		}
	}

	/**
	 * Returns the admin processor.
	 *
	 * @return WidgetAdmin
	 */
	public function bindWidgetAdminProcessor() {
		$plugin      = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$options     = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$adTypes     = $this->container->resolve( 'SheKnows\Infuse\Core\AdTypes' );
		$viewFactory = $this->container->resolve( 'SheKnows\Infuse\Core\ViewFactory' );

		return new WidgetAdmin( $plugin, $options, $adTypes, $viewFactory );
	}

	/**
	 * Returns an instance of the widget public processor (displays an ad).
	 *
	 * @return WidgetPublic
	 */
	public function bindWidgetPublicProcessor() {
		$options      = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$viewFactory  = $this->container->resolve( 'SheKnows\Infuse\Core\ViewFactory' );
		$adTypes      = $this->container->resolve( 'SheKnows\Infuse\Core\AdTypes' );
		$plugin       = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$disableAds   = $this->container->resolve( 'SheKnows\Infuse\Subsystems\HeaderCode\DisableAds' );
		$errorLogging = $this->container->resolve( 'SheKnows\Infuse\Core\ErrorLogging' );

		return new WidgetPublic( $options, $viewFactory, $adTypes, $plugin, $disableAds, $errorLogging );
	}

	/**
	 * Constructs the HelpTab class.
	 *
	 * @return HelpTab
	 */
	public function bindHelpTab() {
		$plugin      = $this->container->resolve( 'SheKnows\Infuse\Plugin' );
		$viewFactory = $this->container->resolve( 'SheKnows\Infuse\Core\ViewFactory' );

		return new HelpTab( $plugin, $viewFactory );
	}

	/**
	 * Constructs tracker class
	 *
	 * @return Tracker
	 */
	public function bindTracker() {
		$options = $this->container->resolve( 'SheKnows\Infuse\Core\Options' );
		$plugin  = $this->container->resolve( 'SheKnows\Infuse\Plugin' );

		return new Tracker( $options, $plugin );
	}

	/**
	 * Add the help tab to the appropriate pages.
	 */
	public function addHelp() {
		$this->container->resolve( __NAMESPACE__ . '\HelpTab' )->show();
	}
}
