<?php

namespace SheKnows\Infuse\Subsystems\AdWidget;

use SheKnows\Infuse\Plugin;
use SheKnows\Infuse\Core\AdTypes;
use SheKnows\Infuse\Core\ErrorLogging;
use SheKnows\Infuse\Core\Options;
use SheKnows\Infuse\Core\UpstreamCommonConfig;
use SheKnows\Infuse\Core\UpstreamSiteConfig;
use SheKnows\Infuse\Core\ViewFactory;
use SheKnows\Infuse\Subsystems\HeaderCode\DisableAds;

/**
 * Class WidgetPublic
 *
 * @package SheKnows\Infuse\Subsystems\Widget
 */
class WidgetPublic {
	/**
	 * @var Options
	 */
	protected $options;

	/**
	 * @var ViewFactory
	 */
	protected $viewFactory;

	/**
	 * @var AdTypes
	 */
	protected $adTypes;

	/**
	 * @var Plugin
	 */
	protected $plugin;

	/**
	 * @var DisableAds
	 */
	protected $disableAds;

	/**
	 * @var ErrorLogging
	 */
	protected $errorLogging;

	/**
	 * WidgetPublic constructor.
	 *
	 * @param Options      $options
	 * @param ViewFactory  $viewFactory
	 */
	public function __construct( Options $options, ViewFactory $viewFactory, AdTypes $adTypes, Plugin $plugin, DisableAds $disableAds, ErrorLogging $errorLogging ) {
		$this->options      = $options;
		$this->viewFactory  = $viewFactory;
		$this->adTypes      = $adTypes;
		$this->plugin       = $plugin;
		$this->disableAds   = $disableAds;
		$this->errorLogging = $errorLogging;
	}

	/**
	 * Constructs and renders a widget ad.
	 *
	 * NOTE: This returns.  The main Widget class is responsible for echoing the
	 *       output.
	 *
	 * @param  array   $args       The widget args.
	 * @param  array   $instance   Widget instance info.
	 * @return string              The rendered widget markup.
	 */
	public function render( $args, $instance ) {
		// Must have a valid type.
		if ( $this->disableAds->getValue() || ! isset( $instance['adtype'] ) || ! $this->adTypes->getTypeByName( $instance['adtype'] ) ) {
			return '';
		}

		$classes = $this->plugin->getSlug() . '-widget-ad';

		if ( isset( $instance['advertisement_callout'] ) ) {
			$classes .= ' ' . $this->plugin->getSlug() . '-ad-callout';
		}

		$vars = array(
			'divid'   => $args['widget_id'] . '-ad',
			'classes' => $classes,
			'type'    => $instance['adtype'],
			'style'   => '',
		);

		$info = $this->adTypes->getTypeByName( $instance['adtype'] );
		if ( ! empty( $info['style'] ) ) {
			$vars['style'] = $info['style'];
		}

		return $this->viewFactory->make( 'ad', $vars )->render();
	}

}
