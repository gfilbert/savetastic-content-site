<?php
/**
 * The default template for displaying single post content (with sidebar).
 * This is a template part. It must be used within The Loop.
 *
 * @package Bimber_Theme 5.4
 */

$bimber_entry_data = bimber_get_template_part_data();
$bimber_elements   = $bimber_entry_data['elements'];
?>

<?php
if ( bimber_show_breadcrumbs() ) :
	bimber_render_breadcrumbs();
endif;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry-tpl-bunchy entry-tpl-classic-v3-no-sidebar' ); ?> itemscope="" itemtype="<?php echo esc_attr( bimber_get_entry_microdata_itemtype() ); ?>">
	<?php get_template_part( 'template-parts/post/sponsor-top' ); ?>

	<header class="entry-header entry-header-03">


		<?php bimber_render_entry_title( '<h1 class="g1-mega g1-mega-1st entry-title" itemprop="headline">', '</h1>' ); ?>

		<?php
		if ( bimber_can_use_plugin( 'wp-subtitle/wp-subtitle.php' ) ) :
			the_subtitle( '<h2 class="entry-subtitle g1-gamma g1-gamma-3rd" itemprop="description">', '</h2>' );
		endif;
		?>

		<?php if ( bimber_can_use_plugin( 'snax/snax.php' ) ) : ?>
			<?php get_template_part( 'template-parts/snax-bar-post' ); ?>
		<?php endif; ?>


	</header>

	<?php
	if ( bimber_show_entry_featured_media( $bimber_elements['featured_media'] ) ) :
		bimber_render_entry_featured_media( array(
			'size'          => 'bimber-classic-1of1',
			'class'         => 'entry-featured-media-main',
			'use_microdata' => true,
			'use_nsfw'      => false,
			'apply_link'    => false,
			'show_caption'  => true,
			'allow_video'   => in_array( get_post_format(), apply_filters( 'bimber_single_featured_media_allow_video',  array() ) ),
			'allow_gif'     => true,
		) );
	endif;
	?>
<?php bimber_render_top_share_buttons(); ?>
	<div class="entry-content g1-typography-xl g1-indent g1-indent-both" itemprop="articleBody">
		<?php 
		add_filter('bimber_use_bunchy_list','__return_true');
		the_content();
		add_filter('bimber_use_bunchy_list','__return_false'); ?>
	</div>

</article>
